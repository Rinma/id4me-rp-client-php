<?php

use Id4me\RP\Service;
use Id4me\RP\HttpClient;
use Id4me\Test\Mock\HttpClientGuzzle;

include __DIR__.'/../vendor/autoload.php';

/**
 * Class describing how to use provided Id4Me mechanisms supported by current library
 */
class Example
{
    /**
     * @var Service
     */
    private $id4Me = null;

    /**
     * @var HttpClient
     */
    protected $httpClient = null;

    /**
     * Initializes Constructor for Example RP Client Class
     */
    public function __construct()
    {
        $httpClient = new HttpClientGuzzle();
        $this->id4Me = new Service($httpClient);
    }

    /**
     * Main function running Id4Me mechanisms supported by current library
     */
    public function run()
    {
        $identifier = readline('identifier:');
        echo PHP_EOL;
        echo '***********************************Discovery***************************************';
        echo PHP_EOL;
        $authorityName = $this->id4Me->discover($identifier);
        var_dump($authorityName);
        echo PHP_EOL;
        echo PHP_EOL;
        echo '***********************************Registration***************************************';
        echo PHP_EOL;
        $openIdConfig = $this->id4Me->getOpenIdConfig($authorityName);
        var_dump($openIdConfig);
        echo PHP_EOL;
        $client = $this->id4Me->register(
            $openIdConfig,
            $identifier,
            sprintf('http://www.rezepte-elster.de/id4me.php', $identifier)
        );
        var_dump($client);
        echo PHP_EOL;
        echo PHP_EOL;
        echo '***********************************Authenticate***************************************';
        echo PHP_EOL;
        echo "Do following steps:\n";
        echo "1.Please click on login link below\n";
        echo "2.Login with password '123456'\n";
        echo "3.Copy and Paste 'code' value from corresponding url query parameter into code input prompt field below'\n";
        $authorizationUrl = $this->id4Me->getAuthorizationUrl(
            $openIdConfig, $client->getClientId(), $identifier, $client->getActiveRedirectUri(), 'idtemp2.id4me.family'
        );
        var_dump($authorizationUrl);
        echo PHP_EOL;
        echo PHP_EOL;
        $authorizedAccessTokens = $this->id4Me->authorize(
            $openIdConfig,
            readline('code:'),
            sprintf('http://www.rezepte-elster.de/id4me.php', $identifier),
            $client->getClientId(),
            $client->getClientSecret()
        );

        var_dump($authorizedAccessTokens);
        echo PHP_EOL;
        echo PHP_EOL;
    }
}

$action = new Example();
$action->run();
