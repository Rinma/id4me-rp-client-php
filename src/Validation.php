<?php

namespace Id4me\RP;

use Id4me\RP\Exception\InvalidAuthorityIssuerException;
use Id4me\RP\Exception\InvalidIDTokenException;
use Id4me\RP\Model\IdToken;

/**
 * This class is responsible of providing utility function in order to validate authority data
 *
 * Following validation use cases are covered here:
 *
 * - ID Token:
 *  solely following requirements of ID4Me specifications 4.5.3. ID Token Validation: 1. to 5. and 9.
 */
class Validation
{
    const INVALID_ID_TOKEN_ISSUER_EXCEPTION = 'iss and issuer values are not equals';
    const INVALID_ID_TOKEN_AUDIENCE_EXCEPTION = 'Authority audience does not contains clientId';
    const INVALID_ID_TOKEN_EXPIRATION_TIME_EXCEPTION = 'Invalid ID Token expiration time';
    const INVALID_ID_TOKEN_SIGNATURE_EXCEPTION = 'Invalid ID Token Signature provided';
    const INVALID_ID_TOKEN_ALGORITHM_EXCEPTION = 'Invalid algorithm: %s, only RS256 supported currently';
    const INVALID_ID_TOKEN_JWS_KEYSET_KEY_NOT_FOUND = 'Used key not found in JWKS for signature verification';
    const INVALID_ID_TOKEN_JWS_KEYSET_STRUCTURE = 'Invalid JWKS structure';
    const INVALID_ID_TOKEN_STRUCTURE = 'Invalid ID Token Structure';

    /**
     * Validates given authority issuer data
     *
     * Note this this the second validation step described in ID4Me specifications of ID Token Validation
     *
     * @param string  $originIssuer
     * @param string  $deliveredIssuer
     * @param boolean $exactMatch
     *
     * @throws InvalidAuthorityIssuerException if provided iss and issuer values are not equal
     */
    public function validateISS(string $originIssuer, string $deliveredIssuer, bool $exactMatch = true): void
    {
        if ($exactMatch && ($originIssuer !== $deliveredIssuer)) {
            throw new InvalidAuthorityIssuerException(self::INVALID_ID_TOKEN_ISSUER_EXCEPTION);
        }

        if (!$exactMatch && !preg_match('/https?:\/\/' . preg_quote($originIssuer, '/') . '\/?/', $deliveredIssuer)) {
            throw new InvalidAuthorityIssuerException(self::INVALID_ID_TOKEN_ISSUER_EXCEPTION);
        }
    }

    /**
     * Validates given ID Token signature using given value and RSA key data
     *
     * @param string  $usedKey
     * @param string  $algorithm
     * @param IdToken $idToken
     * @param array   $jwksArray
     *
     * @throws InvalidIDTokenException
     */
    public function validateIdTokenSignature(
        string $usedKey,
        string $algorithm,
        IdToken $idToken,
        array $jwksArray
    ): void {

        if ($algorithm != 'RS256') {
            throw new InvalidIDTokenException(
                sprintf(self::INVALID_ID_TOKEN_ALGORITHM_EXCEPTION, $algorithm)
            );
        }

        // Retrieve signature and content
        $signature = $idToken->getDecodedSignature();
        $content   = $idToken->getOriginalHeader() . '.' . $idToken->getOriginalBody();

        // Load jwks and init the correct key from 3rd party
        if (array_key_exists('keys', $jwksArray) && is_array($jwksArray['keys'])) {
            foreach ($jwksArray['keys'] as $key) {
                if (isset($key['kid']) && ($key['kid'] == $usedKey)) {
                    $usedKeyDetails = $key;
                    break;
                }
            }
        } else {
            throw new InvalidIDTokenException(self::INVALID_ID_TOKEN_JWS_KEYSET_STRUCTURE);
        }
        if (! isset($usedKeyDetails)) {
            throw new InvalidIDTokenException(self::INVALID_ID_TOKEN_JWS_KEYSET_KEY_NOT_FOUND);
        }
        
        // Generate OpenSSL key resource
        $keyHandle = $idToken->getOpenSslKey($usedKeyDetails);

        // Verify signature with key
        if (1 !== openssl_verify($content, $signature, $keyHandle, 'sha256')) {
            throw new InvalidIDTokenException(self::INVALID_ID_TOKEN_SIGNATURE_EXCEPTION);
        }
    }

    /**
     * Validates audience data in given decrypted token value
     *
     * Validation will be done in accordance to specifications 3., 4. and 5. defined in 4.5.3. ID Token Validation
     *
     * @param mixed       $audience
     * @param string|null $authorizedParty
     * @param string      $clientId
     *
     * @throws InvalidIDTokenException
     */
    public function validateAudience($audience, $authorizedParty, string $clientId): void
    {
        $isValidAudience = false;

        if (! empty($audience)) {
            if (is_array($audience)) {
                $isValidAudience = $this->isValidMultipleAudience($audience, $authorizedParty, $clientId);
            } elseif ($audience == $clientId) {
                $isValidAudience = true;
            } elseif ($audience != $clientId) {
                // check if `azd` matches client-id
                if ($authorizedParty == $clientId) {
                    $isValidAudience = true;
                }
            }
        }

        if (! $isValidAudience) {
            throw new InvalidIDTokenException(self::INVALID_ID_TOKEN_AUDIENCE_EXCEPTION);
        }
    }

    /**
     * Checks if given audience data in constellation of multiple audience (clientId list) are valid
     *
     * In this case verification is done in accordance to specifications 4. and 5. defined in 4.5.3. ID Token Validation
     *
     * @param array  $audience
     * @param string $authorizedParty
     * @param string $clientId
     *
     * @return bool
     */
    private function isValidMultipleAudience(array $audience, string $authorizedParty, string $clientId): bool
    {
        if ((count($audience) <= 1) && (current($audience) == $clientId)) {
            return true;
        }

        if ($authorizedParty && ($authorizedParty != $clientId)) {
            return false;
        }

        return in_array($clientId, $audience);
    }

    /**
     * Validates ID Token expiration time
     *
     * @param string $expirationTime
     *
     * @throws InvalidIDTokenException
     */
    public function validateExpirationTime(string $expirationTime): void
    {
        $isValidExpirationTime = false;

        if (! empty($expirationTime)) {
            $isValidExpirationTime = (time() < intval($expirationTime));
        };

        if (! $isValidExpirationTime) {
            throw new InvalidIDTokenException(self::INVALID_ID_TOKEN_EXPIRATION_TIME_EXCEPTION);
        }
    }
}
